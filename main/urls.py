
from . import views
from django.conf.urls import url


urlpatterns = [
    url(r'^(?i)user/(?P<pk>[.\d]+)/?$', views.User.as_view()),
     url(r'^(?i)user/?$', views.User.as_view()),
    url(r'^(?i)users/?$', views.UsersLists.as_view()),
    url(r'^(?i)user/batch_create/?$', views.UsersFileUpload.as_view()),
]