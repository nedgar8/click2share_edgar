# -*- coding: utf-8 -*-
from main.models import Users
from rest_framework import serializers


class UsersSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Users
        fields = ('id', 'first_name', 'last_name', 'email')
    