from rest_framework import status
from rest_framework.response import Response
from django.db import models

class CustomQuerySetManager(models.Manager):
    def __getattr__(self, attr, *args):
        try:
            return getattr(self.__class__, attr, *args)
        except AttributeError:
            # don't delegate internal methods to the queryset
            if attr.startswith('__') and attr.endswith('__'):
                raise
            return getattr(self.get_queryset(), attr, *args)

    def get_queryset(self):
        result = self.model.QuerySet(self.model)
        return result