# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.query import QuerySet
from main.utils import CustomQuerySetManager

class Users(models.Model):
    first_name = models.CharField(max_length=500)
    last_name = models.CharField(max_length=500)
    email = models.EmailField(max_length=255) # unique=False
    objects = CustomQuerySetManager()
    
    class Meta:
        managed = False
        db_table = 'users'
        
    class QuerySet(QuerySet):
        def filter_by_first_name(self, value):
            if value:
                return self.filter(first_name__icontains=value)
            return self
        
        def filter_by_last_name(self, value):
            if value:
                return self.filter(last_name__icontains=value)
            return self
        
        def filter_by_email(self, value):
            if value:
                return self.filter(email__icontains=value)
            return self
        
        