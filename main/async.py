import threading
from main.models import Users


class UsersUploadFromCSVThread(threading.Thread):
    # http://stackoverflow.com/questions/11632034/async-functions-in-django-views
    def __init__(self, data, *args, **kwargs):
        self.data = data
        super(UsersUploadFromCSVThread, self).__init__(*args, **kwargs)

    def run(self):
        data = self.data
        instances = []
        for record in data:
            user = Users(first_name=record['first_name'],
                         last_name=record['last_name'],
                         email=record['email'])
            instances.append(user)
            
        Users.objects.bulk_create(instances)
        return True