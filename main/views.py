from rest_framework.views import APIView
from django.http.response import Http404
from rest_framework.response import Response
from rest_framework import status
from main.serializers import UsersSerializer
from main.models import Users
from rest_framework.parsers import MultiPartParser, JSONParser
from rest_framework import generics
import base64
import io
from main.async import UsersUploadFromCSVThread

class UsersLists(generics.ListAPIView):
    serializer_class = UsersSerializer

    def get_queryset(self):
        qs = Users.objects.all()
        first_name = self.request.query_params.get('first_name', None)
        last_name = self.request.query_params.get('last_name', None)
        email = self.request.query_params.get('email', None)
        
        qs = qs.filter_by_first_name(first_name).\
                filter_by_last_name(last_name).\
                filter_by_email(email)
        return qs

    
class User(APIView):
    
    def get_object(self, pk):
        try:
            return Users.objects.get(pk=pk)
        except Users.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        snippet = self.get_object(pk)
        serializer = UsersSerializer(snippet)
        return Response(serializer.data)

    def post(self, request):
        serializer = UsersSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def put(self, request, pk):
        user = self.get_object(pk)
        serializer = UsersSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    
class UsersFileUpload(APIView):
    #parser_classes = (MultiPartParser,)
    
    def post(self, request):
#         headers = ['first_name', 'last_name', 'email']
        decoded = base64.b64decode(request.data['encoded_data']).decode('utf-8')
        
        validated_data = []
        for row in decoded.split('\n'):
            first_name, last_name, email = row.split(',')
            data = {'first_name': first_name,
                    'last_name': last_name,
                    'email': email}
            serializer = UsersSerializer(data=data)
            
            # Custom validation
        
            if serializer.is_valid():
                validated_data.append(data)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            
        if validated_data:
            UsersUploadFromCSVThread(validated_data).start()
        return Response()