"""click2sure_edgar URL Configuration

"""
# -*- coding: utf-8 -*-

from django.conf.urls import url, include


urlpatterns = []

urlpatterns += (
    url(r'', include('main.urls')),
)
